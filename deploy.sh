BUCKET_NAME=react-caching-test

# build the assets for deployment
npm run build

# create the bucket with correct config
aws s3api create-bucket --acl public-read --bucket $BUCKET_NAME
aws s3api put-bucket-website --bucket $BUCKET_NAME --website-configuration file://website-config.json

# move files to S3
aws s3 sync --delete build s3://$BUCKET_NAME --acl public-read --cache-control max-age=86400000,public

# set cache-control for index.html to 0
aws s3api copy-object --copy-source $BUCKET_NAME/index.html --bucket $BUCKET_NAME --key index.html --metadata-directive REPLACE --cache-control max-age=0,must-revalidate,public --acl public-read --content-type 'text/html; charset=utf-8'